package com.example.kate.locationapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tvOut;
    TextView tvLon;
    TextView tvLat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvOut = (TextView) findViewById(R.id.textView1);
        tvLon = (TextView) findViewById(R.id.longitude);
        tvLat = (TextView) findViewById(R.id.latitude);
        Log.d("started.", "MainActivity");
        Context context = getApplicationContext();

        //Получаем сервис
        final LocationManager mlocManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        LocationListener mlocListener = new LocationListener() {
            public void onLocationChanged(Location location) {

                //Called when a new location is found by the network location provider.
                tvLat.append(" " + location.getLatitude());
                tvLon.append(" " + location.getLongitude());
                Log.d("location changed.", "mlocListener");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location location = mlocManager.getLastKnownLocation(provider);
                tvLon = (TextView)findViewById(R.id.longitude);
                tvLat = (TextView)findViewById(R.id.latitude);
            }
            public void onProviderDisabled(String provider) {}

        };

        Log.d("onCreate" ,"Before checking permission");
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Подписываемся на изменения в показаниях датчика
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);

        Log.d("onCreate" ,"After checking permission");
        //Если gps включен, то ... , иначе вывести "GPS is not turned on..."
        if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            tvOut.setText("GPS is turned on...");

        } else {
            tvOut.setText("GPS is not turned on...");
        }
    }
}
